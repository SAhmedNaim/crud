<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>CRUD with php OOP & PDO</title>

	<!-- bootstrap cdn css --> 
	<link rel="stylesheet" href="inc/bootstrap.min.css"/>
	
	<!-- google hosted jquery -->
	<script type="text/javascript" src="inc/jquery.min.js"></script>
	
	<!-- bootstrap cdn javascript -->
	<script type="text/javascript" src="inc/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">
		<!-- layout for page header -->
		<div class="well text-center">
			<h2>Dynamic CRUD with php OOP & PDO</h2>
		</div>
