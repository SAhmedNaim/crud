<?php 
	include 'inc/header.php'; 
	include 'lib/Database.php';
?>

<?php
	include 'lib/Session.php';
	Session::init();
	$msg = Session::get('msg');
	if (!empty($msg)) {
		echo '<h2 style="padding:8px 0px; border-radius:10px;" class="alert alert-info text-center">'.$msg.'</h2>';
		Session::sessionUnset();
	} 

?>
<!-- code for page body -->
<div class="panel panel-default">
	<div class="panel-heading">
		<h2>Student List <a class="btn btn-info pull-right" href="addstudent.php">Add Student</a></h2>
	</div>

	<div class="panel-body">
		<table class="table table-striped" style="text-align:center;">
			<tr>
				<th style="text-align:center;">Serial</th>
				<th style="text-align:center;">Name</th>
				<th style="text-align:center;">Email</th>
				<th style="text-align:center;">Phone</th>
				<th style="text-align:center;">Action</th>
			</tr>
	<?php
		$db 		=	new Database();
		$table 		=	"tbl_student";
		$order_by	=	array('order_by' => 'id DESC');
		$selectCond	=	array('select' => 'name');
		$whereCond	=	array(
							'where' 		=> array('id' => '2', 'email' => 'huda@gmail.com'),
							'return_type'	=> 'single'
						);
		// use either case 1 or case 2, but not use that 2 case at a time which is given below
		$limit 		=	array('start' => '2', 'limit' => '3'); // this is case 1
		//$limit 		=	array('limit' => '4'); // this is case 2
	?>













	<?php /* start code block when 'whereCond' is being passed  ?>
	<?php
		$studentData 		=	$db->select($table, $whereCond);
		if (!empty($studentData)) {
			//$i = 0;
			//foreach ($studentData as $data) {
			//	$i++;
	?>
			<tr>
				<td><?php //echo $i; ?></td>
				<td><?php echo $studentData['name']; ?></td>
				<td><?php echo $studentData['email']; ?></td>
				<td><?php echo $studentData['phone']; ?></td>
				<td>
					<a class="btn btn-default" href="editstudent.php?id=<?php echo $studentData['id']; ?>">Edit</a>
					<a class="btn btn-danger" href="process_student.php?action=delete&id=<?php echo $studentData['id']; ?>" onclick="return confirm('Are you sure to delete?');">Delete</a>
				</td>
			</tr>
	<?php
			//}
		} else {
	?>		
			<tr>
				<td colspan="5">
					<h2>No Student Data Found. . .</h2>
				</td>
			</tr>
	<?php
		}
	?>
	<?php //end code block when 'whereCond' is being passed */ ?>
















	<?php // start code block when 'order_by' is being passed ?>
	<?php	
		$studentData 		=	$db->select($table, $order_by);
		if (!empty($studentData)) {
			$i = 0;
			foreach ($studentData as $data) {
				$i++;
	?>
			<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $data['name']; ?></td>
				<td><?php echo $data['email']; ?></td>
				<td><?php echo $data['phone']; ?></td>
				<td>
					<a class="btn btn-default" href="editstudent.php?id=<?php echo $data['id']; ?>">Edit</a>
					<a class="btn btn-danger" href="lib/process_student.php?action=delete&id=<?php echo $data['id']; ?>" onclick="return confirm('Are you sure to delete?');">Delete</a>
				</td>
			</tr>
	<?php
			}
		} else {
	?>		
			<tr>
				<td colspan="5">
					<h2>No Student Data Found. . .</h2>
				</td>
			</tr>
	<?php
		}
	?>
	<?php // end code block when 'order_by' is being passed */ ?>














	<?php /* start code block when 'limit' is being passed ?>
	<?php	
		$studentData 		=	$db->select($table, $limit);
		if (!empty($studentData)) {
			$i = 0;
			foreach ($studentData as $data) {
				$i++;
	?>
			<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $data['name']; ?></td>
				<td><?php echo $data['email']; ?></td>
				<td><?php echo $data['phone']; ?></td>
				<td>
					<a class="btn btn-default" href="editstudent.php?id=<?php echo $data['id']; ?>">Edit</a>
					<a class="btn btn-danger" href="process_student.php?action=delete&id=<?php echo $data['id']; ?>" onclick="return confirm('Are you sure to delete?');">Delete</a>
				</td>
			</tr>
	<?php
			}
		} else {
	?>		
			<tr>
				<td colspan="5">
					<h2>No Student Data Found. . .</h2>
				</td>
			</tr>
	<?php
		}
	?>
	<?php // end code block when 'limit' is being passed */ ?>















		</table>
	</div>
</div>

<?php include 'inc/footer.php'; ?>