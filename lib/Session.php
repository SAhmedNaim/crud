<?php

class Session {
    // initializiting session with checking version
    public static function init() {
        if (version_compare(phpversion(), '5.4.0', '<')) {
            if (session_id() == '') {
                session_start();
            }

        } else {
            if (session_status() == PHP_SESSION_NONE) {
                session_start();
            }
        }
    }
    
    // session is being set using setter
    public static function set($key, $value) {
        $_SESSION['$key'] = $value;
    }
    
    // session is being returned using getter
    public static function get($key) {
        if (isset($_SESSION['$key'])) {
            return $_SESSION['$key'];
        } else {
            return false;
        }
    }
    
    // session unset
    public static function sessionUnset() {
    	session_unset();
    }
    
    
    

}

?>