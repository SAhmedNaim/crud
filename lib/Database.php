<?php

/**
* Database class
*/
class Database {
	private $hostdb	= "localhost";
	private $namedb	= "crud";
	private $userdb	= "root";
	private $passdb	= "";
	private $pdo;

	public function __construct() {
		if (!isset($this->pdo)) {
			try {
				$link = new PDO("mysql:host=".$this->hostdb.";dbname=".$this->namedb, $this->userdb, $this->passdb);
				$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$link->exec("SET CHARACTER SET utf8");
				$this->pdo = $link;
			} catch (PDOException $e) {
				die("Error with database connection! ". $e->getMessage());
			}
		}
	}

	// create data
	public function insert($table, $data) {
		if (!empty($data) && is_array($data)) {
			$keys = '';
			$values = '';

			$keys = implode(',', array_keys($data));
			$values = ":".implode(', :', array_keys($data));
			
			$sql = "INSERT INTO ".$table." (".$keys.") VALUES(".$values.")";
			
			$query = $this->pdo->prepare($sql);

			foreach ($data as $key => $value) {
				$query->bindValue(":$key", $value);
			}

			$insertData = $query->execute();

			if ($insertData) {
				$lastId = $this->pdo->lastInsertId();
				return $lastId;
			} else {
				return false;
			}

		}
	}


	// read data
	public function select($table, $data = array()) {
		/* for the 'read' operation with 'PDO' in php there is to be 4 operations. such as:
		 *	1. sql commandis being assigned to the $sql variable
		 *	2. 'prepare($sql)' is called
		 *	3. 'bindValue(":name", $name)' is called
		 *	4. 'execute'
		 *	5. 'fetch(PDO::FETCH_ASSOC)' or 'fetchAll()' is called
		 *	6. return the assigned variable at last
		 */

		// start - sql command is being assigned in '$sql' dynamically
		$sql	= 'SELECT';
		$sql	.= array_key_exists("select", $data) ? $data['select'] : '*';
		$sql 	.= ' FROM '.$table;
		if (array_key_exists("where", $data)) {
			$sql .= ' WHERE ';
			$i 	= 0;
			foreach ($data['where'] as $key => $value) {
				$add = ($i > 0) ? ' AND ' : '';
				$sql .= "$add"."$key=:$key";
				$i++;
			}
		}

		// code for when 'order by' is being passed
		if (array_key_exists('order_by', $data)) {
			$sql .= ' ORDER BY '.$data['order_by'];
		}

		// code for when 'limit' is being passed
		// this is case 1
		if (array_key_exists('start', $data) && array_key_exists('limit', $data)) {
			$sql .= ' LIMIT '.$data['start'].','.$data['limit'];
		} 
		// this is case 2
		elseif (!array_key_exists('start', $data) && array_key_exists('limit', $data)) {
			$sql .= ' LIMIT '.$data['limit'];
		}
		// end - sql command is being assigned in '$sql' dynamically

		// sql query is now prepared with the method named 'prepare' at 'PDO' api
		$query = $this->pdo->prepare($sql);

		//  'bindValue()' method is now applied for the key named 'where'
		if (array_key_exists("where", $data)) {
			foreach ($data["where"] as $key => $value) {
				$query->bindValue(":$key" ,$value); // in 'bindValue()' method double quote must be used.
			}
		}

		// query execute method is called to execute sql query
		$query->execute();

		// code for 'return_type'
		if (array_key_exists("return_type", $data)) {
			switch ($data["return_type"]) {
				case 'count':
					$value = $query->rowCount();
					break;
				case 'single':
					$value = $query->fetch(PDO::FETCH_ASSOC);
					break;
				
				default:
					$value = '';
					break;
			}
		} else {
			if ($query->rowCount() > 0) {
				$value = $query->fetchAll();
			}
		}
		return !empty($value) ? $value : false;




	}


	// update data
	public function update($table, $data, $condition) {
		if (!empty($data) && is_array($data)) {
			$keys = '';
			$whereCond = '';

			$i = 0;
			foreach ($data as $key => $value) {
				$add = ($i > 0) ? ' , ' : '';
				$keys .= "$add"."$key=:$key";
				$i++;
			}

			if (!empty($condition) && is_array($condition)) {
				$whereCond .= " WHERE ";
				$i = 0;
				foreach ($condition as $key => $value) {
					$add = ($i > 0) ? ' AND ' : '';
					$whereCond .= "$add"."$key=:$key";
					$i++;
				}
			} 

			$sql = "UPDATE ".$table." SET ".$keys.$whereCond;
			$query = $this->pdo->prepare($sql);

			foreach ($data as $key => $value) {
				$query->bindValue(":$key", $value);
			}

			foreach ($condition as $key => $value) {
				$query->bindValue(":$key", $value);
			}

			$update = $query->execute();
			return $update ? $query->rowCount() : false;

		} else {
			return false;
		}

	}


	// delete data
	public function delete($table, $data) {
		if (!empty($data) && is_array($data)) {
			$whereCond = ' WHERE ';

			$i = 0;
			foreach ($data as $key => $value) {
				$add = ($i > 0) ? ' , ' : '';
				//$whereCond .= "$add"."$key=:$key"; // this is for method 1 which is given below
				$whereCond .= $add.$key." = '".$value."'"; // this is for method 2 which is given below
				$i++;
			}
		}

		/* start of method 1 for deletion
		$sql = "DELETE FROM ".$table.$whereCond;

		$query = $this->pdo->prepare($sql);

		foreach ($data as $key => $value) {
			$query->bindValue(":$key", $value);
		}

		$delete = $query->execute();

		return $delete ? true : false;
		end of method 1 for deletion */

		// start of method 2 for deletion 
		$sql = "DELETE FROM ".$table.$whereCond;
		$delete = $this->pdo->exec($sql);
		return $delete ? true : false;		
		// end of method 2 for deletion

	}





}


?>